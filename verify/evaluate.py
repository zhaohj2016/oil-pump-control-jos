import math
import numpy as np
from sympy.core.singleton import S
import torch
import torch.nn as nn
import hyper
from dreal import *
import sympy
from sympy import Matrix
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
from matplotlib.ticker import MultipleLocator
import oilenv_rc


env = oilenv_rc.oilPumpenv()

class Policynn(nn.Module):  # input: n*4 tensor
    def __init__(self, in_dim, hid_dim, out_dim):
        # 定义所有网络层
        super(Policynn, self).__init__()
        self.layer1 = nn.Linear(in_dim, hid_dim)
        self.act1 = nn.ReLU()
        self.layer2 = nn.Linear(hid_dim, out_dim)
        self.act2 = nn.Softmax(dim=1)

    def forward(self, x):
        y = self.layer1(x)
        y = self.act1(y)
        y = self.layer2(y)
        y = self.act2(y)

        return y[:, :-1]  # 返回的action_T 是一个tensor(n, 4)


policy_net = Policynn(hyper.N_VOLUME, hyper.N_P_HID, hyper.N_TIME + 1)

policy_net.load_state_dict(torch.load('policy_end.pt'), strict=True)

q_r_net = nn.Sequential(
    nn.Linear(hyper.N_VOLUME + hyper.N_TIME, hyper.N_Q_r_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_r_HID, hyper.N_Q_r_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_r_HID, 1))

q_c_net = nn.Sequential(
    nn.Linear(hyper.N_VOLUME + hyper.N_TIME, hyper.N_Q_c_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_c_HID, hyper.N_Q_c_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_c_HID, 1))


policy_net.load_state_dict(torch.load('policy_end.pt'), strict=True)
q_c_net.load_state_dict(torch.load('q_c_end.pt'), strict=True)
q_r_net.load_state_dict(torch.load('q_r_end.pt'), strict=True)

def evaluate(epoch, ini, periods):
    state = env.reset_fixed(ini, periods)  # 0-tensor
    init_state = state 
    init_state = init_state.unsqueeze(dim=0).unsqueeze(dim=0)  # tensor: 1*1
    with torch.no_grad():
        # rc_lambda = lagrange_net((init_state - hyper.STATE_CENTER) / hyper.STATE_WIDTH).squeeze_().squeeze_()
        rc_lambda = torch.tensor(0.)
    print(
        "--------------------------------------------------------------------------------------------------------------------------------------")
    print("evaluate initial:", init_state.item())
    fig = plt.figure()
    plt.ion()
    plt.xlabel("Time")
    plt.ylabel("Volume")
    plt.title("volume periods")
    plt.text(150, 31, "epoch: %d\nstate: %.4f\nlmda: %.4f pen: %.4f"
             % (epoch, state, hyper.LAMBDA, hyper.PENALTY_FUN_WEIGHT), color='b',
             bbox={'facecolor': '#74C476',  # 填充色
                   'edgecolor': 'b',  # 外框色
                   'alpha': 0.5,  # 框透明度
                   }
             )

    ax = plt.gca()
    x_major_locator = MultipleLocator(20)
    y_major_locator = MultipleLocator(5)
    ax.xaxis.set_major_locator(x_major_locator)
    ax.yaxis.set_major_locator(y_major_locator)

    plt.xlim(0, env.cycle * env.n_periods)
    plt.ylim(0, 30)
    plt.grid()
    plt.axhline(y=env.min_v, ls=":", c="red", label="safety_lower")
    plt.axhline(y=env.max_v, ls=":", c="green", label="safety_upper")
    plt.legend()
    # plt.ion()

    with torch.no_grad():
        reward = 0
        cost = 0
        step = 0
        while step < env.n_periods:
            step += 1
            state = (state - hyper.STATE_CENTER) / hyper.STATE_WIDTH
            state.unsqueeze_(dim=0)  # tensor: 1
            state.unsqueeze_(dim=0)  # tensor: 1*1
            action_T = policy_net(state)
            sa = torch.cat((state, action_T), dim=1)
            
            # lambda_s = lagrange_net(state).squeeze()
            lambda_s = torch.tensor(0.)
            reward_hat = q_r_net(sa).squeeze()
            cost_hat = q_c_net(sa).squeeze()

            env.render(action_T, lambda_s, reward_hat, cost_hat)  # 输出n_periods个周期油量变化图

            next_state, r, done, done_safety, penalty, _ = env.step(action_T)  # action_T: 1*4
            # next_state: 0-tensor; done: bool; reward, 0-tensor;penalty: 2-tensor; jc: 2-tensor
            
            state = next_state
            reward += r.item()
            cost += penalty.item()
            if done or step == env.n_periods:
                print("evaluate initial:", init_state.item(), "path reward:", reward, "path cost:", cost)
                print(
                    "--------------------------------------------------------------------------------------------------------------------------------------")
                plt.draw()
                plt.pause(50)
                #plt.savefig(os.path.join(result_path, ("fig_%d.png" % epoch)))
                plt.close(fig)
                break  # one episode


# loop_index = np.arange(-98, 100, 1)
# for i in loop_index:
#     ini = (float)(i / 100.0)
#     evaluate(0, ini)

evaluate(0, -1, 100)