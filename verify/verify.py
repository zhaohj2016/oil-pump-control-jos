import math
import numpy as np
from sympy.core.singleton import S
import torch
import torch.nn as nn
import hyper
from dreal import *
import sympy
from sympy import Matrix
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
from matplotlib.ticker import MultipleLocator
import oilenv_rc


env = oilenv_rc.oilPumpenv()

class Policynn(nn.Module):  # input: n*4 tensor
    def __init__(self, in_dim, hid_dim, out_dim):
        # 定义所有网络层
        super(Policynn, self).__init__()
        self.layer1 = nn.Linear(in_dim, hid_dim)
        self.act1 = nn.ReLU()
        self.layer2 = nn.Linear(hid_dim, out_dim)
        self.act2 = nn.Softmax(dim=1)

    def forward(self, x):
        y = self.layer1(x)
        y = self.act1(y)
        y = self.layer2(y)
        y = self.act2(y)

        return y[:, :-1]  # 返回的action_T 是一个tensor(n, 4)


policy_net = Policynn(hyper.N_VOLUME, hyper.N_P_HID, hyper.N_TIME + 1)

policy_net.load_state_dict(torch.load('policy_end.pt'), strict=True)

n_linear_layers = 0
weight_list = []
for p in policy_net.parameters():
    weight_list.append(sympy.Matrix(p.data.detach().numpy()))
    n_linear_layers += 1

input_var = [sympy.symbols("x"+str(i)) for i in range(hyper.N_VOLUME)]

input_matrix = Matrix(input_var) #(1, 1)

hidden_1 = input_matrix * weight_list[0].T + weight_list[1].T #(1, 8)
myrelu=lambda x: sympy.Max(x,0)
hidden_1_act = hidden_1.applyfunc(myrelu) #relu
hidden_2 = hidden_1_act * weight_list[2].T + weight_list[3].T #(1, 5)
myexp = lambda x: sympy.exp(x)
output_exp = hidden_2.applyfunc(myexp) #(1, 5)
exp_sum = output_exp.dot(sympy.ones(1, hyper.N_TIME + 1)) # scaler value


x0 = Variable("x0")
for i in range(hyper.N_TIME):
    exec("t" + str(i) + "=" + "Variable(\"t" + str(i) + "\")")
for i in range(hyper.N_TIME):
    exec("T" + str(i) + "=" + "Variable(\"T" + str(i) + "\")")
for i in range(hyper.N_TIME):
    exec("t_cons" + str(i) + "=" + "(" + str(exp_sum) + ")" + "*" + "t" + str(i) + "==" + str(output_exp[0, i]))

SCALE = 12.0
for i in range(hyper.N_TIME):
    if i == 0:
        exec("T_cons" + str(i) + "=" + "T" + str(i) + "==" + "2" + "+" + str(SCALE) + "*t" + str(i))
    else:
        exec("T_cons" + str(i) + "=" + "T" + str(i) + "==" + "2" + "+" + str(SCALE) + "*t" + str(i) + "+T" + str(i-1))

T_cons_delay = And(*tuple([eval("T_cons" + str(i)) for i in range(hyper.N_TIME)]))
T_cons_softmax = And(*tuple([eval("t_cons" + str(i)) for i in range(hyper.N_TIME)]))
T_cons_all = And(T_cons_delay, T_cons_softmax)

vi = Variable("vi")
vo = Variable("vo")
v = Variable("v")
t = Variable("t")

pump_cons = Or(And(t >= 0, t <= T0, vi == 0), \
                And(t >= T0, t <= T1, vi == 2.2 * (t - T0)), \
                    And(t >= T1, t <= T2, vi == 2.2 * (T1 - T0)), \
                        And(t >= T2, t <= T3, vi == 2.2 * (T1 - T0) + 2.2 * (t - T2)), \
                            And(t >= T3, t <= 20, vi == 2.2 * (T1 + T3 - T0 - T2))
)

consum_cons = Or(And(t >= 0, t <= 2, vo == 0), \
                    And(t >= 2, t <= 4, vo >= 1.1 * (t - 2), vo <= 1.3 * (t - 2)), \
                        And(t >= 4, t <= 8, vo >= 2.2, vo <= 2.6), \
                            And(t >= 8, t <= 10, vo >= 2.2 + 1.1 * (t - 8), vo <= 2.6 + 1.3 * (t - 8)), \
                                And(t >= 10, t <= 12, vo >= 4.4 + 2.4 * (t - 10), vo <= 5.2 + 2.6 * (t - 10)), \
                                    And(t >= 12, t <= 14, vo >= 9.2, vo <= 10.4), \
                                        And(t >= 14, t <= 16, vo >= 9.2 + 1.6 * (t - 14), vo <= 10.4 + 1.8 * (t - 14)), \
                                            And(t >= 16, t <= 18, vo >= 12.4 + 0.4 * (t - 16), vo <= 14 + 0.6 * (t - 16)), \
                                                And(t >= 18, t <= 20, vo >= 13.2, vo <= 15.2) \

)

range_t = And(t >= 0, t <= 20)
end_t = t == 20
safety = Not(And((x0 * 9.9 + 15) + vi - vo >= 5.1, (x0 * 9.9 + 15) + vi - vo <= 24.9)) # x0 \in [-1, 1]; (x0 * 9.9 + 15) \in [5.1, 24,9]
end_range = Not(And((x0 * 9.9 + 15) + vi - vo >= 5.2, (x0 * 9.9 + 15) + vi - vo <= 24.8))

delta = 0.0001
result_box = Box([x0])

# init_volume = And(x0 >= -0.998, x0 <= -0.99)
# safe_cons_all = And(init_volume, range_t, T_cons_all, pump_cons, consum_cons, safety)
# end_cons_all = And(init_volume, end_t, T_cons_all, pump_cons, consum_cons, end_range)
# delta = 0.0001
# result = CheckSatisfiability(safe_cons_all, delta, result_box)
# print(result)
# print(result_box)

# init_volume = And(x0 >= 0.99, x0 <= 0.999)
# result = CheckSatisfiability(And(init_volume, range_t, T_cons_all, pump_cons, consum_cons, safety), 0.0001, result_box)
# print(result)
# print(result_box)

# loop_index = np.arange(-98, 100, 1)
# reversed_index = loop_index[::-1]

# for i in reversed_index:
#     j = i - 1
#     upper = i / 100.0
#     lower = j / 100.0
#     cons_upper = eval("x0 <=" + str(upper) + "+1e-10")
#     cons_lower = eval("x0 >=" + str(lower) + "-1e-10")
#     init_volume = And(cons_upper, cons_lower)
#     result = CheckSatisfiability(And(init_volume, range_t, T_cons_all, pump_cons, consum_cons, safety), 0.0001, result_box)
#     print(lower, upper, result)

loop_index = np.arange(-98, 100, 1)
reversed_index = loop_index[::-1]

all_result = False
for i in reversed_index:
    j = i - 1
    upper = i / 100.0
    lower = j / 100.0
    cons_upper = eval("x0 <=" + str(upper) + "+1e-10")
    cons_lower = eval("x0 >=" + str(lower) + "-1e-10")
    init_volume = And(cons_upper, cons_lower)
    #end_cons_all = And(init_volume, end_t, T_cons_all, pump_cons, consum_cons, end_range)
    #result = CheckSatisfiability(end_cons_all, delta, result_box)
    safe_cons_all = And(init_volume, range_t, T_cons_all, pump_cons, consum_cons, safety)
    result = CheckSatisfiability(safe_cons_all, delta, result_box)
    all_result = result or all_result
    print(lower, upper, result)
print(all_result)