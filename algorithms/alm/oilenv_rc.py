import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
import random
from matplotlib import pyplot as plt
import torch
from scipy.stats import truncnorm

import hyper


class oilPumpenv(gym.Env):
    def __init__(self):
        # 一个周期的时长
        self.cycle = 20
        # 更新的周期数
        self.periods = 0
        # 设置的总周期数
        self.n_periods = hyper.EPOCH_STEPS

        # 计算油量步长
        self.time_step = 0.01
        # P网络输出的时间的范围
        self.min_time = 0
        self.max_time = 20
        self.n_t_steps = round((self.max_time - self.min_time) / self.time_step) + 1  # 2001

        # 油泵消耗的扰动
        self.consume_rate_fluct = 0.1
        # 安全参数初始化
        self.tolerance = 0.0
        self.gap = 0
        self.safe_fluct = 0
        # noise，03-31后期考虑noise的时候再加上，以保证step_oilvolume和render中的noise一致
        self.noise = None

        # 油量安全上下界
        self.safety_lower = hyper.SAFETY_LOWER
        self.safety_upper = hyper.SAFETY_UPPER
        self.reward_penalty1 = hyper.REWARD_PENALTY_SUM
        self.reward_penalty2 = hyper.REWARD_PENALTY_BOUND
        self.reward_penalty3 = hyper.REWARD_PENALTY_END
        self.reward_scale = hyper.REWARD_SCALE_SUM  # 400

        # 初始油量范围
        self.min_v = self.safety_lower
        self.max_v = self.safety_upper  # 7.5

        self.time_space = spaces.Box(
            low=self.min_time, high=self.max_time, shape=(4,))

        # 观测时间范围
        self.seed()
        # 观测状态
        # self.observation_space = spaces.Box(low = 0,high = self.cycle * self.n_periods)

        self.state = None
        self.viewer = None
        self.steps_beyond_done = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step_oilvolume(self, action_T):  # action_T: 1*4 tensor
        done_safety = 0

        volume = self.state  # 0-tensor
        volume_upper = volume
        volume_lower = volume

        v_index = 1
        yout = torch.zeros(self.n_t_steps)
        yout_upper = torch.zeros(self.n_t_steps)  # volume upper bound
        yout_lower = torch.zeros(self.n_t_steps)  # volume lower bound

        yout[0] = volume
        yout_lower[0] = volume_lower
        yout_upper[0] = volume_upper

        for t in np.arange(self.min_time, self.max_time, self.time_step):
            # temp = random.random()
            # noise = self.consume_rate_fluct * (2 * temp - 1)
            noise = 0

            # consume rate
            if 0 <= t and t < 2:
                consume_rate = 0
            elif 2 <= t and t < 4:
                consume_rate = 1.2 + noise
            elif 4 <= t and t < 8:
                consume_rate = 0
            elif 8 <= t and t < 10:
                consume_rate = 1.2 + noise
            elif 10 <= t and t < 12:
                consume_rate = 2.5 + noise
            elif 12 <= t and t < 14:
                consume_rate = 0
            elif 14 <= t and t < 16:
                consume_rate = 1.7 + noise
            elif 16 <= t and t < 18:
                consume_rate = 0.5 + noise
            elif 18 <= t and t <= 20:
                consume_rate = 0
            else:
                consume_rate = 0

            if consume_rate == 0:
                consume_rate_upper = 0
                consume_rate_lower = 0
            else:
                consume_rate_upper = consume_rate + self.consume_rate_fluct  # consume rate upper bound
                consume_rate_lower = consume_rate - self.consume_rate_fluct  # consume rate lower bound

            if t < action_T[:, 0]:
                pump_rate = 0
            elif action_T[:, 0] <= t and t < action_T[:, 1]:
                pump_rate = 2.2
            elif action_T[:, 1] <= t and t < action_T[:, 2]:
                pump_rate = 0
            elif action_T[:, 2] <= t and t < action_T[:, 3]:
                pump_rate = 2.2
            elif action_T[:, 3] <= t:
                pump_rate = 0
            else:
                pump_rate = 0

            volume = volume + (pump_rate - consume_rate) * self.time_step
            # consume rate is smaller,volume is bigger
            volume_upper = volume_upper + (pump_rate - consume_rate_lower) * self.time_step
            volume_lower = volume_lower + (pump_rate - consume_rate_upper) * self.time_step

            if volume_upper > self.safety_upper + self.tolerance - self.safe_fluct or \
                    volume_lower < self.safety_lower - self.tolerance + self.safe_fluct:
                done_safety = 1

            yout[v_index] = volume
            yout_upper[v_index] = volume_upper
            yout_lower[v_index] = volume_lower

            v_index = v_index + 1

        # safety_vio_upper = torch.relu(torch.max(yout_upper) - self.safety_upper)  # tensor
        # safety_vio_lower = torch.relu(self.safety_lower - torch.min(yout_lower))  # tensor
        safety_vio_upper = torch.max(yout_upper) - self.safety_upper  # tensor
        safety_vio_lower = self.safety_lower - torch.min(yout_lower)  # tensor
        # upper = max(yout_upper)     # tensor: 0
        # lower = -min(yout_lower)    # tensor: 0
        # beta = torch.tensor(hyper.BETA, dtype=torch.float)
        # set no violation to bound number
        # if done_safety:
        #     upper = beta[0] if upper < beta[0] else upper
        #     lower = beta[1] if lower < beta[1] else lower
        # else:
        #     upper = beta[0]
        #     lower = beta[1]

        state_terminal = yout[-1]
        yout_below = self.reward_penalty1 * torch.relu(self.safety_lower - yout)
        yout_above = torch.relu(yout - self.safety_lower)
        yout = yout_below + yout_above + self.safety_lower

        s_volume = self.time_step * torch.sum(yout) \
                   - 0.5 * self.time_step * (yout[0] + yout[-1])  # tensor

        return (state_terminal, s_volume, done_safety, safety_vio_upper, safety_vio_lower)

    def nn2ctrl(self, output):
        output = 12 * (output)

        output[:, 0] = 2 + output[:, 0]
        output[:, 1] = 2 + output[:, 0] + output[:, 1]
        output[:, 2] = 2 + output[:, 1] + output[:, 2]
        output[:, 3] = 2 + output[:, 2] + output[:, 3]
        return output

    def step(self, action_T):
        action_T = self.nn2ctrl(action_T)  # 1*4 tensor

        self.periods += 1
        (self.state, s_volume, done_safety, vio_upper, vio_lower) = self.step_oilvolume(action_T)
        # (0-tensor, 0-tensor, int, 0-tensor, 0-tensor, 0-tensor, 0-tensor)

        done_periods = bool(self.n_periods == self.periods)  # 走了self.n_periods步，设置done为true
        done_safety = bool(done_safety)

        if hyper.SAFETY_STOP:
            done = done_safety
        else:
            done = False

        # reward = self.reward_scale / s_volume 
        # if done_safety:
        #     reward = reward - self.reward_penalty * (vio_lower + vio_upper) #tensor

        reward = (self.safety_upper * self.cycle - s_volume) / self.reward_scale - self.reward_penalty3 * torch.abs(
            self.state - self.safety_lower)

        #penalty = self.reward_penalty2 * (vio_lower + vio_upper)  # tensor
        penalty = self.reward_penalty2 * torch.relu(torch.max(vio_lower, vio_upper))  # tensor

        return self.state, reward, done, done_safety, penalty, {}  # 0-tensor, 0-tensor, bool, bool, 1-tensor, 1-tensor

    def reset_bias(self, turns, std_scale, periods):  # state: 0 dimensional tensor
        turn = np.random.randint(turns)
        if turn == 0:
            my_mu = self.min_v
            my_sigma = (self.max_v - self.min_v) / std_scale
            my_lower = self.min_v
            my_upper = self.max_v
            trunc_norm = truncnorm((my_lower - my_mu) / my_sigma, (my_upper - my_mu) / my_sigma, loc=my_mu,
                                   scale=my_sigma)
            self.state = torch.tensor(trunc_norm.rvs(1), dtype=torch.float).squeeze()
        else:
            self.state = torch.tensor(self.np_random.uniform(low=self.min_v, high=self.max_v))

        self.n_periods = periods
        self.periods = 0
        return self.state

    def reset_uniform(self):  # state: 0 dimensional tensor
        self.state = torch.tensor(self.np_random.uniform(low=self.min_v, high=self.max_v))

        self.n_periods = hyper.EPOCH_STEPS
        self.periods = 0
        return self.state

    def render(self, action_T, lambda_s, reward_hat, cost_hat):  # 每隔一段时间画出油量变化曲线图
        action_T = self.nn2ctrl(action_T)  # action_T: 1*4 tensor
        print("volume:", self.state.item(), "evaluate action:", action_T.detach().numpy()[0],
              "lambda:", hyper.LAMBDA, "penalty_fun:", hyper.PENALTY_FUN_WEIGHT, "predict reward:", reward_hat.item(), "predict cost:", cost_hat.item())

        volume = self.state  # 0-tensor

        v_index = 1
        yout = torch.zeros(self.n_t_steps)
        yout[0] = volume

        for t in np.arange(self.min_time, self.max_time, self.time_step):
            temp = random.random()
            noise = self.consume_rate_fluct * (2 * temp - 1)
            # consume rate
            if 0 <= t and t < 2:
                consume_rate = 0
            elif 2 <= t and t < 4:
                consume_rate = 1.2 + noise
            elif 4 <= t and t < 8:
                consume_rate = 0
            elif 8 <= t and t < 10:
                consume_rate = 1.2 + noise
            elif 10 <= t and t < 12:
                consume_rate = 2.5 + noise
            elif 12 <= t and t < 14:
                consume_rate = 0
            elif 14 <= t and t < 16:
                consume_rate = 1.7 + noise
            elif 16 <= t and t < 18:
                consume_rate = 0.5 + noise
            elif 18 <= t and t <= 20:
                consume_rate = 0
            else:
                consume_rate = 0

            if t < action_T[:, 0]:
                pump_rate = 0
            elif action_T[:, 0] <= t and t < action_T[:, 1]:
                pump_rate = 2.2
            elif action_T[:, 1] <= t and t < action_T[:, 2]:
                pump_rate = 0
            elif action_T[:, 2] <= t and t < action_T[:, 3]:
                pump_rate = 2.2
            elif action_T[:, 3] <= t:
                pump_rate = 0
            else:
                pump_rate = 0

            volume = volume + (pump_rate - consume_rate) * self.time_step
            yout[v_index] = volume
            v_index = v_index + 1

        plt.plot(
            np.arange(self.periods * self.cycle, (self.periods + 1) * self.cycle + self.time_step / 10, self.time_step),
            yout)

        plt.show()
        plt.pause(0.1)


'''obsolete resest
    # def reset(self, bias, std_scale): #state: 0 dimensional tensor
    #     # turn = np.random.randint(bias)
    #     # if turn == 0:
    #     #     my_mu = self.min_v
    #     #     my_sigma = (self.max_v - self.min_v) / std_scale
    #     #     my_lower = self.min_v
    #     #     my_upper = self.max_v
    #     #     trunc_norm = truncnorm((my_lower - my_mu) / my_sigma, (my_upper - my_mu) / my_sigma, loc=my_mu, scale=my_sigma)
    #     #     self.state = torch.tensor(trunc_norm.rvs(1), dtype=torch.float).squeeze()
    #     # elif turn == 1:
    #     #     my_mu = self.max_v
    #     #     my_sigma = (self.max_v - self.min_v) / std_scale
    #     #     my_lower = self.min_v
    #     #     my_upper = self.max_v
    #     #     trunc_norm = truncnorm((my_lower - my_mu) / my_sigma, (my_upper - my_mu) / my_sigma, loc=my_mu, scale=my_sigma)
    #     #     self.state = torch.tensor(trunc_norm.rvs(1), dtype=torch.float).squeeze()
    #     # else:
    #     #     self.state = torch.tensor(self.np_random.uniform(low = self.min_v, high = self.max_v))

    #     # if np.random.rand() < bias:
    #     #     my_mu = self.min_v
    #     # else:
    #     #     my_mu = self.max_v
    #     # my_sigma = (self.max_v - self.min_v) / std_scale
    #     # my_lower = self.min_v
    #     # my_upper = self.max_v
    #     # trunc_norm = truncnorm((my_lower - my_mu) / my_sigma, (my_upper - my_mu) / my_sigma, loc=my_mu, scale=my_sigma)
    #     # self.state = torch.tensor(trunc_norm.rvs(1), dtype=torch.float).squeeze()

    #     self.state = torch.tensor(self.np_random.uniform(low = self.min_v, high = self.max_v))

    #     self.periods = 0
    #     self.n_periods = 10
    #     return self.state 
'''
