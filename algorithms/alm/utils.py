import os
import glob
import imageio

def images_to_gif(path, suffix, savename =None):
    img_array = []

    imgList = glob.glob(os.path.join(path,suffix))
    imgList.sort(key = lambda x: int(x.split("fig_")[1][:-4])) # sorted by name
    for filename in imgList:
        img = imageio.imread(filename)
        if img is None:
            print(filename + " is error!")
            continue
        img_array.append(img)

    if savename is None:
        savename = "train"
    imageio.mimsave(os.path.join(path,savename+'.gif'), img_array, 'GIF', fps=2)


path = os.getcwd()
train_epoch = 1
train_path = os.path.join(path, "result" + str(train_epoch))
if not os.path.exists(train_path):
    os.mkdir(train_path)

# images_to_gif(train_path,"*.png","train")

