from torch.nn.modules import loss
import oilenv_rc
import gym
import math
import random
import os
import numpy as np
import pandas as pd
import heapq
import matplotlib
import matplotlib.pyplot as plt
# matplotlib.use('TkAgg')
from matplotlib.ticker import MultipleLocator
from collections import namedtuple
from collections import deque
from itertools import count
from PIL import Image
from copy import deepcopy
import time
import imageio

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
# import torchvision.transforms as T
import hyper
import utils


# ############################################
# # set default data type to double; for GPU
# # training use float
# ############################################
# torch.set_default_dtype(torch.float64)
# torch.set_default_tensor_type(torch.DoubleTensor)


'''1. 测试环境初始化并输出初始化值'''
env = oilenv_rc.oilPumpenv()

# state = env.reset_uniform()
# print("initial state: ", state)
# print("state type: ",type(state)) #state为tensor
# print(state.size)
# time = torch.tensor(env.time_space.sample())
# print("sample time: ", time)


# P网络的输出时间维度及其范围
# n_time = env.time_space.shape[0]
# print("#action: ", n_time)
# print(env.time_space)
# print(env.time_space.high)
# print(env.time_space.low)


'''3.1. 经验池'''
Experience = namedtuple('Experience', ('state', 'action_T', 'reward', 'next_state', 'terminal', 'penalty'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque(maxlen=capacity)

    def push(self, *args):
        self.memory.append(Experience(*args))  ## append a new experience

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):  ## len(experience)
        return len(self.memory)


experience_pool = ReplayMemory(hyper.MEMORY_SIZE)  # initialize memory pool

'''5. Policy network & Q network
#注：两个网络输出的均为向量tensor类型
'''


class Policynn(nn.Module):  # input: n*4 tensor
    def __init__(self, in_dim, hid_dim, out_dim):
        # 定义所有网络层
        super(Policynn, self).__init__()
        self.layer1 = nn.Linear(in_dim, hid_dim)
        self.act1 = nn.ReLU()
        self.layer2 = nn.Linear(hid_dim, out_dim)
        self.act2 = nn.Softmax(dim=1)

    def forward(self, x):
        y = self.layer1(x)
        y = self.act1(y)
        y = self.layer2(y)
        y = self.act2(y)

        return y[:, :-1]  # 返回的action_T 是一个tensor(n, 4)


policy_net = Policynn(hyper.N_VOLUME, hyper.N_P_HID, hyper.N_TIME + 1)

q_r_net = nn.Sequential(
    nn.Linear(hyper.N_VOLUME + hyper.N_TIME, hyper.N_Q_r_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_r_HID, hyper.N_Q_r_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_r_HID, 1))

q_c_net = nn.Sequential(
    nn.Linear(hyper.N_VOLUME + hyper.N_TIME, hyper.N_Q_c_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_c_HID, hyper.N_Q_c_HID),
    nn.ReLU(),
    nn.Linear(hyper.N_Q_c_HID, 1))


policy_net.load_state_dict(torch.load('policy_end.pt'), strict=True)
q_c_net.load_state_dict(torch.load('q_c_end.pt'), strict=True)
q_r_net.load_state_dict(torch.load('q_r_end.pt'), strict=True)

target_p_net = deepcopy(policy_net)
target_q_r_net = deepcopy(q_r_net)
target_q_c_net = deepcopy(q_c_net)

def enable_gradient(network):
    for p in network.parameters():
        p.requires_grad = True


def disable_gradient(network):
    for p in network.parameters():
        p.requires_grad = False


disable_gradient(target_p_net)
disable_gradient(target_q_r_net)
disable_gradient(target_q_c_net)

def copy_net(source_net, target_net):
    with torch.no_grad():
        # 软更新target net的权重参数
        for p, p_targ in zip(source_net.parameters(), target_net.parameters()):
            p_targ.data.mul_(hyper.UPDATE_WEIGHT)
            p_targ.data.add_((1 - hyper.UPDATE_WEIGHT) * p.data)


'''6. 探索'''


def explore_noise(state, epoch):  # tensor: 1*1
    with torch.no_grad():
        # noise = hyper.EXPLORE_NOISE_UPPER - (epoch * hyper.EPOCH_STEPS / 10.0) * (
        #         hyper.EXPLORE_NOISE_UPPER - hyper.EXPLORE_NOISE_LOWER) / hyper.EPOCHS
        # if epoch % 10 == 0:
        #     noise = hyper.MAX_NOISE
        # else:
        # noise = hyper.EXPLORE_NOISE_UPPER - (epoch * 1.0) * (
        #         hyper.EXPLORE_NOISE_UPPER - hyper.EXPLORE_NOISE_LOWER) / hyper.EPOCHS

        policy_t = policy_net(state)
        nnoutput = policy_t
        # while True:
        #     nnoutput = torch.normal(mean=policy_t, std=noise)  # tensor: 1*4
        #     nnoutput = torch.clamp(nnoutput, 0.0, 1.0)
        #     if (torch.sum(nnoutput) <= 1).item():
                # break
    return nnoutput


def explore_one_step(state, epoch):  # tensor: 1*1
    action_T = explore_noise(state, epoch)  # tensor: 1*4

    obs, r, done, done_safety, penalty, _ = env.step(action_T)  # 0-tensor, 0-tensor, bool
    terminal = torch.tensor(int(done) * 1.0)  # 0-tensor
    obs = (obs - hyper.STATE_CENTER) / hyper.STATE_WIDTH

    experience_pool.push(state.squeeze().squeeze(), action_T.squeeze(), r, obs, terminal,
                         penalty)  # (s,a,r,s',t), tensors
    return done, done_safety, obs, r, penalty  # bool, 0-tensor, 0-tensor


'''7. 优化更新'''
optimizer_p = optim.Adam(policy_net.parameters(), lr=hyper.LEARN_RATE_ACTIC, weight_decay=0.00)
optimizer_q_r = optim.Adam(q_r_net.parameters(), lr=hyper.LEARN_RATE_CRITIC_R, weight_decay=0.00)
optimizer_q_c = optim.Adam(q_c_net.parameters(), lr=hyper.LEARN_RATE_CRITIC_C, weight_decay=0.00)
loss_fn = torch.nn.MSELoss()


def sample_batch():
    experiences = experience_pool.sample(hyper.BATCH_SIZE)
    # [Experience(state=.., action_T=.., ...),Experience(state=..)]
    experiences_batch = Experience(*zip(*experiences))  # experiences of batches, unpack twice

    state_batch = torch.stack(experiences_batch.state).unsqueeze(dim=1)  # batchsize * 1
    action_T_batch = torch.stack(experiences_batch.action_T)  # batchsize * 4
    reward_batch = torch.stack(experiences_batch.reward)  # batchsize
    penalty_batch = torch.stack(experiences_batch.penalty)  # batchsize
    next_state_batch = torch.stack(experiences_batch.next_state).unsqueeze(dim=1)  # batchsize * 4
    terminal_batch = torch.stack(experiences_batch.terminal)  # batchsize
    state_action_T_batch = torch.cat((state_batch, action_T_batch), dim=1)  # batchsize * 5

    return state_batch, action_T_batch, reward_batch, penalty_batch, next_state_batch, terminal_batch, state_action_T_batch


def update_q_r_net(r, ns, d, sa):
    # r: batchsize; pen: batchsize * 2; ns: batchsize * 1; d: batchsize; sa: batchsize * 4;
    curr_q_r_value = q_r_net(sa).squeeze()

    n_action = target_p_net(ns)  # target p net
    # n_action = torch.normal(mean=n_action, std=hyper.TARGET_SMOOTH_NOISE)  # tensor: 1*4
    # n_action = torch.clamp(n_action, 0.0, 1.0)
    next_sa = torch.cat((ns, n_action), dim=1)

    target_next_q_r_value = target_q_r_net(next_sa).squeeze()  # target q net

    target_q_r_value = r + hyper.GAMMA * target_next_q_r_value * (1 - d)
    target_q_r_value = target_q_r_value.squeeze()

    # mean square loss
    loss = loss_fn(curr_q_r_value, target_q_r_value)

    # Optimize the model
    optimizer_q_r.zero_grad()
    loss.backward()
    optimizer_q_r.step()

    return loss.item()


def update_q_c_net(pen, ns, d, sa):
    # pen: batchsize * 2; ns: batchsize * 1; d: batchsize; sa: batchsize * 4;
    curr_q_c_value = q_c_net(sa).squeeze()

    n_action = target_p_net(ns)  # target p net
    # n_action = torch.normal(mean=n_action, std=hyper.TARGET_SMOOTH_NOISE)  # tensor: 1*4
    # n_action = torch.clamp(n_action, 0.0, 1.0)
    next_sa = torch.cat((ns, n_action), dim=1)

    target_next_q_c_value = torch.relu(target_q_c_net(next_sa).squeeze())  # target q net

    target_q_c_value = pen + hyper.GAMMA * target_next_q_c_value * (1 - d)
    target_q_c_value = target_q_c_value.squeeze()
    
    # s_test = torch.tensor([[0.5]])
    # a_test = policy_net(s_test)
    # sa_test = torch.cat((s_test, a_test), dim=1)
    # print(q_c_net(sa_test).item())

    # mean square loss
    loss = loss_fn(curr_q_c_value, target_q_c_value)

    # Optimize the model
    optimizer_q_c.zero_grad()
    loss.backward()
    optimizer_q_c.step()

    return loss.item()


def update_lagrange_net(s, total_steps):  # s: batchsize * 1
    # using qc network, qr network and lagrange network
    
    s = s[s[:, 0] > -0.99]
    s = s[s[:, 0] < 0.99]
    if len(s) > 0:
        disable_gradient(q_c_net)
        disable_gradient(policy_net)

        curr_action_T = policy_net(s)                   # batchsize * 4
        curr_sa = torch.cat((s, curr_action_T), dim=1)  # batchsize * 5

        q_c_value = q_c_net(curr_sa).squeeze()

        q_c_value, max_ind = torch.max(q_c_value, 0)
        print("current state and qc:", (s.squeeze())[max_ind].item(), ", ", q_c_value.item())

        current_qc = torch.mean(torch.relu(q_c_value)).item()
        hyper.LAMBDA += hyper.LEARN_RATE_LAGRANGE * current_qc

        # if current_qc >= 0.05:
        #     hyper.PENALTY_FUN_WEIGHT = min(hyper.PENALTY_FUN_WEIGHT * hyper.PENALTY_SCALE, 100.0)
        # else:
        #     hyper.PENALTY_FUN_WEIGHT = hyper.LEARN_RATE_LAGRANGE
        #     hyper.PENALTY_SCALE = 1.0
        #     hyper.PENALTY_GAP = 1
        #     hyper.EPOCH_STEPS = 10
        #     hyper.EXPLORE_NOISE_LOWER = 0.04
        #     hyper.MAX_NOISE = 0.16

        enable_gradient(q_c_net)
        enable_gradient(policy_net)

        return current_qc
    else:
        return 0


def update_policy_net(s):  # s: batchsize * 1;
    s = s[s[:, 0] > -0.99]
    s = s[s[:, 0] < 0.99]
    if len(s) > 0:
        curr_action_T = policy_net(s)                   # batchsize * 4
        curr_sa = torch.cat((s, curr_action_T), dim=1)  # batchsize * 5

        # using qc network, qr network and lagrange network
        # disable_gradient(q_r_net)
        disable_gradient(q_c_net)

        # rc_lambda = lagrange_net(s).squeeze()       # batchsize
        # rc_lambda = torch.tensor(0.)
        q_c_value = q_c_net(curr_sa).squeeze()
        # q_r_value = q_r_net(curr_sa).squeeze()

        q_c = torch.mean(q_c_value)
        # q_r = torch.mean(q_r_value)
        # rc_lambda_mean = torch.mean(rc_lambda)


        q_c_value, max_ind = torch.max(q_c_value, 0)
        # q_r_value = q_r_value[max_ind]

        # weight_r = 0
    
        print("current state and qc:", (s.squeeze())[max_ind].item(), ", ", q_c_value.item())

        q_c_value = 0.0 * q_c_value if q_c_value < 0.02 else q_c_value
        #q_filter = q_c_value > 0
        # loss_fun = -1.0 * torch.mean(weight_r * q_r_value)
        # loss_pen = hyper.LAMBDA * torch.mean(torch.relu(q_c_value))
        # loss = loss_pen
        loss = q_c_value

        # Optimize the model
        optimizer_p.zero_grad()
        loss.backward()
        optimizer_p.step()

        # enable_gradient(q_r_net)
        enable_gradient(q_c_net)

        return loss.item(), q_c.item(), 0, 0
    else:
        return 0, 0, 0, 0


'''8. 画图'''

def evaluate(epoch):
    state = env.reset_bias(2, 30, 10)  # 0-tensor
    init_state = state
    init_state = init_state.unsqueeze(dim=0).unsqueeze(dim=0)  # tensor: 1*1
    with torch.no_grad():
        # rc_lambda = lagrange_net((init_state - hyper.STATE_CENTER) / hyper.STATE_WIDTH).squeeze_().squeeze_()
        rc_lambda = torch.tensor(0.)
    print(
        "--------------------------------------------------------------------------------------------------------------------------------------")
    print("evaluate initial:", init_state.item())
    fig = plt.figure()
    plt.ion()
    plt.xlabel("Time")
    plt.ylabel("Volume")
    plt.title("volume periods")
    plt.text(150, 31, "epoch: %d\nstate: %.4f\nlmda: %.4f pen: %.4f"
             % (epoch, state, hyper.LAMBDA, hyper.PENALTY_FUN_WEIGHT), color='b',
             bbox={'facecolor': '#74C476',  # 填充色
                   'edgecolor': 'b',  # 外框色
                   'alpha': 0.5,  # 框透明度
                   }
             )

    ax = plt.gca()
    x_major_locator = MultipleLocator(20)
    y_major_locator = MultipleLocator(5)
    ax.xaxis.set_major_locator(x_major_locator)
    ax.yaxis.set_major_locator(y_major_locator)

    plt.xlim(0, env.cycle * env.n_periods)
    plt.ylim(0, 30)
    plt.grid()
    plt.axhline(y=env.min_v, ls=":", c="red", label="safety_lower")
    plt.axhline(y=env.max_v, ls=":", c="green", label="safety_upper")
    plt.legend()
    # plt.ion()

    with torch.no_grad():
        reward = 0
        cost = 0
        step = 0
        while step < env.n_periods:
            step += 1
            state = (state - hyper.STATE_CENTER) / hyper.STATE_WIDTH
            state.unsqueeze_(dim=0)  # tensor: 1
            state.unsqueeze_(dim=0)  # tensor: 1*1
            action_T = policy_net(state)
            sa = torch.cat((state, action_T), dim=1)
            
            # lambda_s = lagrange_net(state).squeeze()
            lambda_s = torch.tensor(0.)
            reward_hat = q_r_net(sa).squeeze()
            cost_hat = q_c_net(sa).squeeze()

            env.render(action_T, lambda_s, reward_hat, cost_hat)  # 输出n_periods个周期油量变化图

            next_state, r, done, done_safety, penalty, _ = env.step(action_T)  # action_T: 1*4
            # next_state: 0-tensor; done: bool; reward, 0-tensor;penalty: 2-tensor; jc: 2-tensor
            
            state = next_state
            reward += r.item()
            cost += penalty.item()
            if done or step == env.n_periods:
                print("evaluate initial:", init_state.item(), "path reward:", reward, "path cost:", cost)
                print(
                    "--------------------------------------------------------------------------------------------------------------------------------------")
                plt.draw()
                plt.pause(0.3)
                #plt.savefig(os.path.join(result_path, ("fig_%d.png" % epoch)))
                plt.close(fig)
                break  # one episode



'''9. 训练循环'''

def train_loop(result_path):
    datas = []
    epoch = 0
    total_steps = 0
    while epoch < hyper.EPOCHS:
        print("epoch", epoch)
        explore_steps = 0
        reward = 0
        cost = 0
        ds = False

        init_state = env.reset_uniform()
        state = (init_state - hyper.STATE_CENTER) / hyper.STATE_WIDTH  # tensor: 0 dimensional

        state.unsqueeze_(dim=0)  # tensor: 1
        state.unsqueeze_(dim=0)  # tensor: 1*1

        # update lambda old, 2021.05.18 cancel this update method
        '''if len(constraint_pool) > hyper.CONSTRAINT_WARM_UP_SIZE:
            constraint_batch = constraint_sample_batch()
            rc_lambda = torch.relu(
                rc_lambda + hyper.LEARN_RATE_ALPHA * (constraint_batch - torch.tensor(hyper.BETA, dtype=torch.float)))
        '''
        
        epoch += 1

        while explore_steps < env.n_periods:

            # generate experience
            done, done_safety, next_state, r, penalty = explore_one_step(state, epoch)  # bool, 0-tensor, 0-tensor
            state = next_state.unsqueeze(dim=0)
            state = state.unsqueeze_(dim=0)
            reward += r.item()
            cost += penalty.item()
            ds |= done_safety
            explore_steps += 1
            total_steps += 1

            # Perform one step of the optimization
            if len(experience_pool) > hyper.WARM_UP_SIZE:
                s, a, r, pen, ns, d, sa = sample_batch()
                if epoch < 10000:
                    # update p-net and q-net
                    # loss_q_r = update_q_r_net(r, ns, d, sa)
                    loss_q_c = update_q_c_net(pen, ns, d, sa)
                    # copy_net(q_r_net, target_q_r_net)
                    copy_net(q_c_net, target_q_c_net)
                else:
                    loss_p, q_c, q_r, rc_lambda_mean = update_policy_net(s)
                    # loss_lagrange = update_lagrange_net(s, total_steps)
                    # copy_net(policy_net, target_p_net)

            if done:
                break  # one episode

        # if epoch % 100 == 0 and len(experience_pool) > hyper.WARM_UP_SIZE:
        #     print("epoch:", epoch, "reward:", reward, "cost:", cost,
        #           "explore_steps:", explore_steps, "init state:", init_state.item(),
        #           "loss_policy:", loss_p, "lp_update_qc", q_c, "lp_update_qr", q_r, "lp_update_lambda", rc_lambda_mean, "loss_q_r:", loss_q_r, "loss_q_c:", loss_q_c,
        #           "loss_lagrange: ", loss_lagrange)
        # if total_steps % 10 == 0:
        #     epoch += 1
        # if total_steps % 100 == 0:
        #     evaluate(epoch)
    # datas_pd = pd.DataFrame(np.array(datas), columns=['epoch', 'reward', 'penalty', 'lambda', 'penalty_fun_weight'])
    # datas_pd.to_csv(os.path.join(result_path, 'alm.csv'))
    # print("lambda:", hyper.LAMBDA)

path = os.getcwd()
train_epoch = 0
# print('train epoch:', train_epoch)

train_path = os.path.join(path, "result" + str(train_epoch))
if not os.path.exists(train_path):
    os.mkdir(train_path)

start_time = time.time()

# start training
train_loop(train_path)

# save net parameters
torch.save(policy_net.state_dict(), os.path.join(train_path, 'policy_end.pt'))
torch.save(q_r_net.state_dict(), os.path.join(train_path, 'q_r_end.pt'))
torch.save(q_c_net.state_dict(), os.path.join(train_path, 'q_c_end.pt'))
# torch.save(lagrange_net.state_dict(), os.path.join(train_path, 'lagrange_end.pt'))

end_time = time.time()

print("train time:", end_time-start_time)

# training change gif
# utils.images_to_gif(train_path, "*.png", "train")
