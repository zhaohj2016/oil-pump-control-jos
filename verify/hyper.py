## 超参

# p网络和q网络输入层、输出层和隐藏层的维度
N_VOLUME = 1   # 输入为油量
N_TIME = 4     # 输出为时间（4维）
N_P_HID = 8   # p网络的隐藏层
N_Q_r_HID = 128   # qc网络的隐藏层
N_Q_c_HID = 64   # qc网络的隐藏层
N_lambda_HID_1 = 8   # Lagrange网络的隐藏成1
N_lambda_HID_2 = 8   # Lagrange网络的隐藏成2

MEMORY_SIZE = int(1e6)


#--------------------------------------------------
EPOCHS = 50000
EPOCH_STEPS = 1  # next set this to 10
BATCH_SIZE = 128      # batch-train
CONSTRAINT_BATCH_SIZE = 1    # penalty-batch-train
WARM_UP_SIZE = BATCH_SIZE
CONSTRAINT_WARM_UP_SIZE = 16
GAMMA = 0.9           # reward-discount: 根据1/(1-GAMMA)=10或者GAMMA**10=0.1
POLICY_DELAY = 1
TARGET_SMOOTH_NOISE = 0.01

EXPLORE_NOISE_LOWER = 0.08      # 探索噪声的方差，the best choice? 看到有的使用了0.04
EXPLORE_NOISE_UPPER = 0.08
UPDATE_WEIGHT = 0.995       # 用于target-net的软更新

# ALPHA1 < LEARN_RATE_ACTIC < LEARN_RATE_CRITIC
LEARN_RATE_LAGRANGE = 1e-4       # lambda更新系数，参考RCPO论文p13,目前更新为论文中的10倍
PENALTY_FUN_WEIGHT = 1
PENALTY_SCALE = 1.05
PENALTY_GAP = 50
LAMBDA = 0


LEARN_RATE_ACTIC = 5e-4
LEARN_RATE_CRITIC_R = 1e-3
LEARN_RATE_CRITIC_C = 5e-3
LAGRANGE_BOUND = 1

SAFETY_UPPER = 24.9
SAFETY_LOWER = 5.1
BETA = 0.0
MAX_V = SAFETY_UPPER
MIN_V = SAFETY_LOWER
STATE_WIDTH = (MAX_V - MIN_V) / 2.0
STATE_CENTER = (MAX_V + MIN_V) / 2.0

REWARD_PENALTY_SUM = 1.0
REWARD_PENALTY_BOUND = 1.0
REWARD_PENALTY_END = 0 #may be smaller
REWARD_SCALE_SUM = 50.0
SAFETY_STOP = False
