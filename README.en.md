# OilPumpControl-JOS

#### Description
Source code for the JOS paper:  

Zhao HJ, Li QZ, Zeng X, Liu ZM. Safe Reinforcement Learning Algorithm and Its Application in Intelligent Control 
for CPS. Ruan Jian Xue Bao/Journal of Software, 2022, 33(7): 2538−2561 (in Chinese). http://www.jos.org.cn/1000-9825/6588.htm